package com.calculator.democalculator;


import org.junit.jupiter.api.Test;
import org.mockito.Spy;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.extension.ExtendWith;


@ExtendWith(MockitoExtension.class)
public class StringCalculatorTest {
    @Spy
    StringCalculator stringCalculator = new StringCalculator();

    @Test
    void testEmptyCase() {
        String numbers = "";
        int result = stringCalculator.add(numbers);
        assertThat(result).isZero();
    }

    @Test
    void testOneCase() {
        String numbers = "1";
        int result = stringCalculator.add(numbers);
        assertThat(result).isEqualTo(1);
    }

    @Test
    void testOneTwoCase() {
        String numbers = "1,2";
        int result = stringCalculator.add(numbers);
        assertThat(result).isEqualTo(3);
    }

    @Test
    void testUnknownAmountOfNumbersCase() {
        String numbers = "11,2,4,5,9";
        int result = stringCalculator.add(numbers);
        assertThat(result).isEqualTo(31);
    }

    @Test
    void testNewLinesBetweenNumbersCase() {
        String numbers = "1\n2,3";
        int result = stringCalculator.add(numbers);
        assertThat(result).isEqualTo(6);
    }

    @Test
    void testDifferentDelimitersCase() {

        String numbers = "//;\n1;2";
        int result = stringCalculator.add(numbers);
        assertThat(result).isEqualTo(3);
    }

    @Test
    void testNegativeNumberCase() {
        String numbers = "//;\n-1;";
        int result = stringCalculator.add(numbers);
        assertThat(result).isEqualTo(-1);
    }

    @Test
    void testMultipleNegativesCase() {
        try {
            String numbers = "//;\n-1;-2";
            int result = stringCalculator.add(numbers);
            assertThat(result).isEqualTo(-3);
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage()).isEqualTo("negatives not allowed -1 -2");
        }
    }

    @Test
    void testCalledCount() {
        String numbers = "//;\n1;2";
        int result1 = stringCalculator.add(numbers);
        verify(stringCalculator, times(1)).add(number);
    }

    @Test
    void testNullCase() {
        int result = stringCalculator.add(null);
        assertThat(result).isZero();
    }

}

