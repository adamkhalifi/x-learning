package com.calculator.democalculator;

public class StringCalculator {
    public int add(String numbers) {
        if (numbers == null) {
            return 0;
        }
        int sum = 0;
        int parsedNumber = 0;
        StringBuilder sbr = new StringBuilder();
        String[] splt = numbers.split("[/;\\n,]");
        for (int i = 0; i < splt.length; i++) {
            if (splt[i].length() != 0) {
                parsedNumber = Integer.parseInt(splt[i]);
                if (parsedNumber > 0) {
                    sum += parsedNumber;
                } else {
                    sbr.append(parsedNumber).append(" ");
                }
            }
        }
        if (sbr.length() > 0) {
            throw new IllegalStateException("negatives not allowed : " + sbr);
        }
        return sum;
    }

}
