
 C'est quoi Spring boot ?
 
 Spring Boot est un framework de développement JAVA. C'est une déclinaison du framework classique de Spring qui permet essentiellement de réaliser des microservices, ce sont la majeure partie du temps des services web qui sont regroupés en API

- La manière de créer un projet spring à l’aide de spring initializr :
    Créer le nom du projet
    Le langage de programmation (java)
    Choisir spring boot
    Préciser les dependencies

- La structure de créer backend {
        *Controller, 
        *Dto, 
        *Service, 
        *Model 
       }

- Comment utiliser différentes méthodes REST {GET, POST, PUT et DELETE }

- Les trois étapes de TDD (Test-Driven Development ) :
    *Écrire un test unitaire qui échoue. => Red
    *Écrire le code qui permet de réussir le test. ✅
    *Nettoyer le code tout en gardant les tests en succès. => Refactoring.
    @Écrire le prochain test et recommencez !

- L' annotation @Spy : est utilisée pour créer un objet réel et espionner cet objet réel . Un espion aide à appeler toutes les méthodes normales de l'objet tout en suivant chaque interaction, comme nous le ferions avec une simulation.
     
- @ExtendWith(MockitoExtension.class):
L'extension prend en charge les paramètres du constructeur de JUnit Jupiter. Cela vous permet d'effectuer un travail de configuration dans le constructeur et de définir vos champs sur final.

     assertThat : est l'une des méthodes JUnit de l'objet Assert qui peut être utilisée pour vérifier si une valeur spécifique correspond à celle attendue.

- StringBuilder() : Il construit un générateur de chaîne vierge d'une capacité de 16 caractères
- StringBuilder append  : Cette méthode ajoute la chaîne mentionnée à la chaîne existante. Vous pouvez également utiliser des arguments tels que boolean, char, int, double, float, etc.
- La méthode string split(): coupe une chaîne donnée autour des correspondances de l'expression régulière donnée. Après avoir fractionné l'expression régulière donnée, cette méthode renvoie un tableau de chaînes.
- Maven : Maven vous permet de declarer toutes les dependances que vous voulez dans un seul fichier (pom.xml) ce fichier contient la liste de toutes les dependances. 